package com.example.trade;

import com.example.trade.model.Trade;
import net.minidev.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class TradeRestControllerTests {

    @Autowired
    private TestRestTemplate restTemplate;

    // Testing insert new trade
    @Test
    public void testAddNewTrade(){

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        JSONObject userJsonObject = new JSONObject();
        userJsonObject.put("ticker","AAPL");
        userJsonObject.put("quantity","50");
        userJsonObject.put("price","2.5");

        HttpEntity<String> request =
                new HttpEntity<String>(userJsonObject.toString(), headers);

        ResponseEntity<String> responseEntity = restTemplate.exchange("/addNewTrade", HttpMethod.POST,request,String.class);
        assertEquals(HttpStatus.CREATED , responseEntity.getStatusCode());
    }

    // Testing get all trades
    @Test
    public void testGetAllTrades(){

        ResponseEntity<List<Trade>> responseEntity = restTemplate.exchange("/trades", HttpMethod.GET,null,
                new ParameterizedTypeReference<List<Trade>>(){});

        List<Trade>responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
        assertEquals(3, responseBody.size ()); // expected 3
    }

    // Testing get trade by id
    @Test
    public void testGetTradeById(){

        ResponseEntity<List<Trade>> responseEntity = restTemplate.exchange("/trade/1", HttpMethod.GET,null,
                new ParameterizedTypeReference<List<Trade>>(){});

        List<Trade>responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
        assertEquals(0, responseBody.size ()); // expected 0
    }

    // Testing get trades by state
    @Test
    public void testGetTradesByState(){

        ResponseEntity<List<Trade>> responseEntity = restTemplate.exchange("/trade/state/REJECTED", HttpMethod.GET,null,
                new ParameterizedTypeReference<List<Trade>>(){});

        List<Trade>responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
        assertEquals(0, responseBody.size ()); // expected 0
    }

    // Testing get trades by ticker
    @Test
    public void testGetTradesByTicker(){

        ResponseEntity<List<Trade>> responseEntity = restTemplate.exchange("/trade/ticker/C", HttpMethod.GET,null,
                new ParameterizedTypeReference<List<Trade>>(){});

        List<Trade>responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
        assertEquals(0, responseBody.size ()); // expected 0
    }

    // Testing get trades by type
    @Test
    public void testGetTradesByType(){

        ResponseEntity<List<Trade>> responseEntity = restTemplate.exchange("/trade/type/BUY", HttpMethod.GET,null,
                new ParameterizedTypeReference<List<Trade>>(){});

        List<Trade>responseBody = responseEntity.getBody();
        assertEquals(HttpStatus.OK , responseEntity.getStatusCode());
        assertEquals(2, responseBody.size ()); // expected 2
    }
}
