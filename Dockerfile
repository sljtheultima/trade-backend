FROM openjdk:11.0-jre-slim

ADD target/trade-0.0.1-SNAPSHOT.jar app.jar

EXPOSE 8081

ENV TRADE_URI=mongodb://mymongodb:27017/tradedb

#Comment this line if running on docker, needed for openshift
RUN sh -c 'echo spring.data.mongodb.uri=mongodb://tradedb:27017/trade > application.properties'
RUN sh -c 'echo apiinfo.key_name: x-rapidapi-key >> application.properties'
RUN sh -c 'echo apiinfo.key_value = 5560265dfemshf23ba00d498f9e3p17a803jsn7823f6677424 >> application.properties'
RUN sh -c 'echo apiinfo.url = https://yahoo-finance15.p.rapidapi.com/api/yahoo/qu/quote/ >> application.properties'

ENTRYPOINT ["java", "-jar", "/app.jar"]
